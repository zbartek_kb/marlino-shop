
jQuery(document).ready(function($){
        
    
    var $hotline = jQuery('<div id="hotline" >asdasd</div>');        
    
    $hotline.appendTo(jQuery('body'));
            
    $hotline.on('mouseenter click', function(e) {
        jQuery($hotline).animate({'right': '0'});
    });
    
    $hotline.on('mouseleave click', function(e) {        
        
        if($hotline.css('right') === '0px') {
            jQuery($hotline).animate({'right': '-277px'});
        }
    });    
    
    $('form.variations_form.cart').on( 'submit', function(e) {
             
        // if user has seen popup before
        if($('.single_add_to_cart_button').data('seenpopup') == 'true') {
            return true;
        }   
        
        
        var $content = jQuery('<div style="padding: 15px;"><h3>Hinweis:</h3><p>Sind Sie sich sicher, dass diese Größe Ihrer Konfektionsgröße entspricht. Bitte beachten <br />Sie, dass unsere Artikel weit ausfallen und überprüfen Sie nochmals nachstehende Größentabelle:</p><br /><br /></div>');
        var $imgGents = jQuery('<img src="http://www.marlino.de/shop/wp-content/uploads/2013/12/masstabelle_herren.png" style="float: left;" />');
        var $imgLadies = jQuery('<img src="http://www.marlino.de/shop/wp-content/uploads/2013/12/masstabelle_damen.png" style="float: left; margin-bottom: 2em;" /><div style="clear: both" ></div>');
        var $submitButton = jQuery('<a href="#" class="button" style="display: inline-block; margin-right: 1%; width: 45%; background-color: #73be5f; color: #fff; font-size: 16px; text-shadow: none; color: #fff;" >In den Warenkorb legen</a>');
        var $abortButton = jQuery('<a href="#" class="button" style="display: inline-block; width: 44%; font-size: 16px;" >Größe anpassen</a>');
        
        $imgGents.appendTo($content);
        $imgLadies.appendTo($content);
        $submitButton.appendTo($content);
        $abortButton.appendTo($content);
        
        var $popup = $.fancybox({
            'autoScale': true,
            'transitionIn': 'elastic',
            'transitionOut': 'elastic',
            'speedIn': 500,
            'speedOut': 300,
            'autoDimensions': true,
            'centerOnScroll': true,
            'type' : 'inline',
            'content' : $content
        });                
        
        $abortButton.click(function(e) {            
            e.preventDefault();
            $('.single_add_to_cart_button').removeAttr('disabled');
            $('.single_add_to_cart_button').data('seenpopup', 'true');
            $.fancybox.close();
        });
        
        $submitButton.click(function() {
            e.preventDefault();
            $('.single_add_to_cart_button').data('seenpopup', 'true');
            $('.single_add_to_cart_button').removeAttr('disabled');
            $('.single_add_to_cart_button').trigger('click');
        });
               
        return false;
        
    });
});