<?php
/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache



/**
 * In dieser Datei werden die Grundeinstellungen f�r WordPress vorgenommen.
 *
 * Zu diesen Einstellungen geh�ren: MySQL-Zugangsdaten, Tabellenpr�fix,
 * Secret-Keys, Sprache und ABSPATH. Mehr Informationen zur wp-config.php gibt es auf der {@link http://codex.wordpress.org/Editing_wp-config.php
 * wp-config.php editieren} Seite im Codex. Die Informationen f�r die MySQL-Datenbank bekommst du von deinem Webhoster.
 *
 * Diese Datei wird von der wp-config.php-Erzeugungsroutine verwendet. Sie wird ausgef�hrt, wenn noch keine wp-config.php (aber eine wp-config-sample.php) vorhanden ist,
 * und die Installationsroutine (/wp-admin/install.php) aufgerufen wird.
 * Man kann aber auch direkt in dieser Datei alle Eingaben vornehmen und sie von wp-config-sample.php in wp-config.php umbenennen und die Installation starten.
 *
 * @package WordPress
 */

/** neu Strato - 21.09.2016 **/

/**  MySQL Einstellungen - diese Angaben bekommst du von deinem Webhoster. */
/**  Ersetze database_name_here mit dem Namen der Datenbank, die du verwenden m�chtest. */
define('DB_NAME', 'DB2708924');

/** Ersetze username_here mit deinem MySQL-Datenbank-Benutzernamen */
define('DB_USER', 'U2708924');

/** Ersetze password_here mit deinem MySQL-Passwort */
define('DB_PASSWORD', 'wp_shop_2016!');

/** Ersetze localhost mit der MySQL-Serveradresse */
define('DB_HOST', 'rdbms.strato.de');

/** Der Datenbankzeichensatz der beim Erstellen der Datenbanktabellen verwendet werden soll */
define('DB_CHARSET', 'utf8');

/** Der collate type sollte nicht ge�ndert werden */
define('DB_COLLATE', '');

/**#@+
 * Sicherheitsschl�ssel
 *
 * �ndere jeden KEY in eine beliebige, m�glichst einzigartige Phrase. 
 * Auf der Seite {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service} kannst du dir alle KEYS generieren lassen.
 * Bitte trage f�r jeden KEY eine eigene Phrase ein. Du kannst die Schl�ssel jederzeit wieder �ndern, alle angemeldeten Benutzer m�ssen sich danach erneut anmelden.
 *
 * @seit 2.6.0
 */
define('AUTH_KEY',         '1WQ02FNY0#)Mv%g+jCr63K&T$BD@+^&!);~S5;0i|m q]b~rMgC-9MrPZ}2JrcjY');
define('SECURE_AUTH_KEY',  ':rxc-iz1A!;fJRRuoq-)/mT00+5!p!,B/;/}|;q uRfWT%-E&J|#! m_(f+3FAik');
define('LOGGED_IN_KEY',    'p*/fi~>4[C]%yT>[f9__#J|1xc,KpQ`8h1o><6_I&T5*%^FS.y9B[z6h&ASTBf,g');
define('NONCE_KEY',        'Oj^+k8:u_H0yn$(#]$eTsZ2wa: a5I+s_}JQo9/32%|X}vX/^=Cx|7z6ud&p%Q2|');
define('AUTH_SALT',        '-:LztDL{^EF^QFAVT?,/p~z*X4UOw}hLcj^4VE-?h,gjtq=^8bL|Tvl-QmVy_c>,');
define('SECURE_AUTH_SALT', '7]5<?Apu(B))@eIxSec?Y9ij:5Lts=>u eaFQ6cJ8mqJZAyrxs14ze}Z=U|%|Nl.');
define('LOGGED_IN_SALT',   'toNnKR>Yjc+VKYCUQ^S{1=H,k$;$c~H7ji!9j+|:JNhv>_Ap(J%kx`]lYA64#MBN');
define('NONCE_SALT',       'Ruw3/r:*MRC^Kb]2fS6@cBWaNr5o!Vkwd-_olwP*:0}.)TEt@!+)CEz-_$JU:M-,');

/**#@-*/

define('WP_ALLOW_REPAIR', true);

#define('WP_HOME', 'http://www.marlino.de/shop'); // no trailing slash
#define('WP_SITEURL', 'http://www.marlino.de/shop');  // no trailing slash

define('WP_MEMORY_LIMIT', '256M');


/**
 * WordPress Datenbanktabellen-Pr�fix
 *
 *  Wenn du verschiedene Pr�fixe benutzt, kannst du innerhalb einer Datenbank
 *  verschiedene WordPress-Installationen betreiben. Nur Zahlen, Buchstaben und Unterstriche bitte!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Sprachdatei
 *
 * Hier kannst du einstellen, welche Sprachdatei benutzt werden soll. Die entsprechende
 * Sprachdatei muss im Ordner wp-content/languages vorhanden sein, beispielsweise de_DE.mo
 * Wenn du nichts eintr�gst, wird Englisch genommen.
 */
define('WPLANG', 'de_DE');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
//define('WP_DEBUG', false);

/* https://aristath.github.io/blog/wp-hide-php-errors */
ini_set('log_errors','Off');
ini_set('display_errors','Off');
ini_set('error_reporting', E_ALL & ~E_NOTICE);
//ini_set('error_reporting',error_reporting(0));
define('WP_DEBUG', false);
define('WP_DEBUG_LOG', false);
define('WP_DEBUG_DISPLAY', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');