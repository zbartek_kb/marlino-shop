var gulp = require('gulp');
var sass = require('gulp-sass');
var minifyCSS = require('gulp-minify');
var autoprefixer = require('gulp-autoprefixer');
var concat = require('gulp-concat');


gulp.task('sass-styles', function(){
    return gulp.src('app/wp-content/themes/replete/scss/style.scss')
        .pipe(sass())
        .pipe(autoprefixer())
        .pipe(minifyCSS())
        .pipe(concat('style.css'))
        .pipe(gulp.dest('app/wp-content/themes/replete/css/new_design/'))
});
gulp.task('watch',['sass-styles'],function () {
        return gulp.watch(['app/wp-content/themes/replete/scss/*.scss','themes/replete/css/new_design/style.css'],['sass-styles']);
});
gulp.task('default', [ 'sass-styles' ]);